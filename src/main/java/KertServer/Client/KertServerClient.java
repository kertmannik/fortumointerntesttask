package KertServer.Client;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

public class KertServerClient {

    public static void main(String[] args) throws Exception {
        System.out.println(postMessageToServer("Today is very cold and foggy day.", "http://localhost:8080/KertServer/echo"));
        System.out.println(getMessageFromServer("http://localhost:8080/KertServer/report/json"));
    }

    public static String postMessageToServer(String message, String url) throws Exception {
        final OkHttpClient client = new OkHttpClient();
        final RequestBody body = RequestBody.create(MediaType.parse("text/plain"), message);
        final Request request = new Request.Builder()
                .url(url)
                .addHeader("x-candidate", "Kert")
                .post(body)
                .build();

        final Response response = client.newCall(request).execute();
        final int code = response.code();
        if (code >= 200 && code < 300) {
           return response.body().string();
        } else {
            return "Response code is not OK!";
        }
    }

    public static String getMessageFromServer(String url) throws IOException{
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .addHeader("x-candidate", "Kert")
                .build();
        final Response response = client.newCall(request).execute();
        final int code = response.code();
        if (code >= 200 && code < 300) {
            return response.body().string();
        } else {
            return "Response code is not OK!";
        }
    }
}
