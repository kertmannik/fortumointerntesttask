package ee.kert.internexercise;

import ee.kert.internexercise.Application;
import ee.kert.internexercise.Calculation;
import ee.kert.internexercise.Client;
import ee.kert.internexercise.RawDataParser;

public class Main {

    public static void main(String[] args) {
        final Application application = new Application();
        application.doStuff(new Client(), new RawDataParser(), new Calculation());
    }
}
