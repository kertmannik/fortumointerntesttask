package ee.kert.internexercise;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Client {

    public static final String EMPTY_JSON_ARRAY = "[]";

    public String makeHttpRequest(String url) {
        try {
            final String resultOkHttp = this.run(url);
            return resultOkHttp;
        } catch (IOException e) {
            return EMPTY_JSON_ARRAY;
        }
    }

    private String run(String url) throws IOException {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .addHeader("x-candidate", "Kert")
                .build();

        final Response response = client.newCall(request).execute();
        System.out.println("HTTP responds " + response.code());
        final Set<String> headerNames = response.headers().names();
        final List<String> headerNamesList = new ArrayList<>(headerNames);
        for (int i = 0; i < headerNamesList.size(); i++) {
            System.out.println(headerNamesList.get(i) + " " + response.header(headerNamesList.get(i)));
        }
        System.out.println("HTTP response code " + response.code());
        if (response.isSuccessful()) {
            return response.body().string();
        } else {
            return EMPTY_JSON_ARRAY;
        }
    }
}
