package ee.kert.internexercise;

public class Trader {

    private String city;
    private String name;

    public Trader() {
    }

    public String getCity() {
        return this.city;
    }

    public String getName() {
        return this.name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }
}
