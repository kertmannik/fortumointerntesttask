package ee.kert.internexercise;

public class Application {

    public static final int YEAR_OF_INTEREST = 2016;
    public static final String CITY_OF_INTEREST = "Tartu";
    public static final String LARS_LAPTOP_SERVER = "http://192.168.1.49:8080/fortumo-test-task-backend/hr/task";
    private Client clientOkHttp;
    private RawDataParser gson;
    private Calculation tartuTransactionsSum;

    public void doStuff(Client clientOkHttp, RawDataParser gson, Calculation tartuTransactionsSum) {
        this.clientOkHttp = clientOkHttp;
        this.gson = gson;
        this.tartuTransactionsSum = tartuTransactionsSum;

        final String rawData = this.clientOkHttp.makeHttpRequest(LARS_LAPTOP_SERVER);
        final Transaction[] jsonArray = this.gson.rawDataToJsonArray(rawData);

        final int transactionsSum = this.tartuTransactionsSum.transactionsSum(
                jsonArray, YEAR_OF_INTEREST, CITY_OF_INTEREST);

        if (transactionsSum == 0) {
            System.out.print("ee.kert.internexercise.Transaction list is empty.");
        } else {
            System.out.print("The sum of transactions made in " + CITY_OF_INTEREST + " in " + YEAR_OF_INTEREST
                    + " is " + transactionsSum + ".");
        }
    }
}
