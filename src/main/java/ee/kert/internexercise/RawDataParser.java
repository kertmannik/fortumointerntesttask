package ee.kert.internexercise;

import com.google.gson.Gson;

public class RawDataParser {

    public Transaction[] rawDataToJsonArray(String rawData) {
        final Gson gson = new Gson();
        final Transaction[] transactionsList = gson.fromJson(rawData, Transaction[].class);
        return transactionsList;
    }
}
