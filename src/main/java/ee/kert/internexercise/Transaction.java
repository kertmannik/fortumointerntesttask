package ee.kert.internexercise;

public class Transaction {

    private Trader trader;
    private int year;
    private int value;

    public Transaction() {
    }

    public Trader getTrader() {
        return this.trader;
    }

    public int getYear() {
        return this.year;
    }

    public int getValue() {
        return this.value;
    }

    public void setTrader(Trader trader) {
        this.trader = trader;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
