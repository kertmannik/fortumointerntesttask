package ee.kert.internexercise;

import java.util.Arrays;

public class Calculation {

    public static final int YEAR_OF_INTEREST = 2016;

    public int transactionsSum(Transaction[] transactionsList) {
        return Arrays.stream(transactionsList)
                     .filter(transaction -> transaction.getYear() == YEAR_OF_INTEREST)
                     .filter(transaction -> "Tartu".equals(transaction.getTrader().getCity()))
                     .mapToInt(Transaction::getValue)
                     .sum();
    }

    public int transactionsSum(Transaction[] transactionsList, int year, String city) {
        return Arrays.stream(transactionsList).filter(transaction -> transaction.getYear() == year)
                .filter(transaction -> city.equals(transaction.getTrader().getCity()))
                .mapToInt(Transaction::getValue)
                .sum();
    }
}
