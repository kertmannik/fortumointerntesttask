import ee.kert.internexercise.RawDataParser;
import ee.kert.internexercise.Transaction;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RawDataParserTest {
    private RawDataParser parser;

    @Test
    public void Should_pass_json_to_array(){
        //given
        parser = new RawDataParser();
        String rawData = "[{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1000,\"year\":2015},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":800,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":500,\"year\":2014},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":100,\"year\":2014},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":600,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":1000,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":500,\"year\":2016},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":300,\"year\":2015},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":600,\"year\":2016}]";
        //when
        Transaction[] jsonArray = parser.rawDataToJsonArray(rawData);
        //then
        assertThat(jsonArray.length).isEqualTo(11);
        assertThat(jsonArray[0].getYear()).isEqualTo(2015);
        assertThat(jsonArray[0].getTrader().getCity()).isEqualTo("Tartu");
        assertThat(jsonArray[0].getValue()).isEqualTo(1000);
        assertThat(jsonArray[0].getTrader().getName()).isEqualTo("Andrea");
    }
}
