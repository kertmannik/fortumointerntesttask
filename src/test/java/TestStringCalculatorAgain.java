import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestStringCalculatorAgain {

    public int add(String numbers) {
        List<String> splitNumbers = parseString(numbers);
        List<String> negativeNumbers =
                splitNumbers.stream().filter(s -> Integer.parseInt(s) < 0).collect(Collectors.toList());
        if (!negativeNumbers.isEmpty()) {
            throw new IllegalArgumentException("Negative numbers are not allowed!");
        }
        return splitNumbers.stream().mapToInt(s -> Integer.parseInt(s)).filter(n -> n < 1000).sum();
    }

    private List<String> parseString(String numbers) {
        List<String> splitNumbers = new ArrayList<>();
        final String delimiter = determineDelimiter(numbers);
        if (numbers.startsWith("//")) {
            numbers = numbers.split("\n")[1];
        }
        if (numbers.contains("\n")) {
            numbers = numbers.replace("\n", ",");
        }
        if (numbers.contains(delimiter)) {
            splitNumbers = Arrays.asList(numbers.split(delimiter));
        } else {
            splitNumbers = Arrays.asList(numbers);
        }
        return splitNumbers;
    }

    private String determineDelimiter(String numbers) {
        if (numbers.startsWith("//")) {
            return Character.toString(numbers.charAt(2));
        } else {
            return ",";
        }
    }

    @Test
    public void should_return_sum_if_two_numbers_are_given() {
        //given
        String numbers = "6,9";
        //when
        int result = add(numbers);
        //then
        assertEquals(15, result);
    }

    @Test
    public void should_return_the_number_if_only_one_number_is_given() {
        //given
        String number = "15";
        //when
        int result = add(number);
        //then
        assertEquals(15, result);
    }

    @Test
    public void should_return_sum_if_newline_in_numbers_string() {
        //given
        String numbers = "1\n2,3";
        //when
        int result = add(numbers);
        //then
        assertEquals(6, result);
    }

    @Test
    public void should_return_sum_if_custom_delimiter_is_used() {
        //given
        String numbers = "//;\n1;2";
        //when
        int result = add(numbers);
        //then
        assertEquals(3, result);
    }

    @Test
    public void should_throw_exception_when_negative_integers() throws Exception {
        // given
        String numbers = "1\n2,-3";
        try {
            //when
            int result = add(numbers);

            //then
            Assert.fail();
        } catch (IllegalArgumentException exception) {
        }
    }
}
