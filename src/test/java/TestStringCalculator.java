import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TestStringCalculator {

    private StringCalculator stringCalculator;

    static class StringCalculator {

        public static final int DEFAULT_VALUE = 0;

        public int add(String input) {
            if (input.isEmpty()) {
                return DEFAULT_VALUE;
            }
            List<String> splittedInput = parseInput(input);

            List<String> negativeIntegers = splittedInput
                    .stream()
                    .filter(string -> Integer.parseInt(string) < 0)
                    .collect(Collectors.toList());
            if (!negativeIntegers.isEmpty()) {
                throw new IllegalArgumentException("Negative integers not allowed!");
            }
            return splittedInput.stream().mapToInt(s -> Integer.parseInt(s)).filter(i -> i < 1000).sum();
        }

        public List<String> parseInput(String input) {
            List<String> integers = new ArrayList<>();
            String delimiter = determineDelimiter(input);
            if (delimiter.equals(",")) {
                String numbersWithoutLinebreaks = input.replace("\n", ",");
                integers.addAll(Arrays.asList(numbersWithoutLinebreaks.split(delimiter)));
            } else {
                String numbersPart = input.replace("//" + delimiter + "\n", "");
                if (numbersPart.contains("\n")){
                    String replacingNewline = numbersPart.replace("\n", delimiter);
                    integers.addAll(Arrays.asList(replacingNewline.split(delimiter)));
                } else {
                    integers.addAll(Arrays.asList(numbersPart.split(delimiter)));
                }
            }
            return integers;
        }

        private String determineDelimiter(String input) {
            if (input.startsWith("//")) {
                return String.valueOf(input.charAt(2));
            } else {
                return ",";
            }
        }
    }

    @Test
    public void should_return_0_if_empty_input() {
        //given
        this.stringCalculator = new StringCalculator();
        String empty = "";
        //when
        int result = stringCalculator.add(empty);
        //then
        assertEquals(0, result);
    }

    @Test
    public void should_return_the_integer_if_only_one_integer_is_given() {
        //given
        this.stringCalculator = new StringCalculator();
        String seven = "7";
        //when
        int result = stringCalculator.add(seven);
        //then
        assertEquals(7, result);
    }

    @Test
    public void should_return_the_sum_of_two_integers_if_2_integers_are_given() {
        //given
        this.stringCalculator = new StringCalculator();
        String twoIntegers = "6,9";
        //when
        int result = stringCalculator.add(twoIntegers);
        //then
        assertEquals(15, result);
    }

    @Test
    public void should_return_the_sum_of_three_integers_if_3_integers_are_given() {
        //given
        this.stringCalculator = new StringCalculator();
        String threeIntegers = "4,5,6";
        //when
        int result = stringCalculator.add(threeIntegers);
        //then
        assertEquals(15, result);
    }

    @Test
    public void should_return_the_sum_of_integers_while_ignoring_integers_bigger_1000() {
        //given
        this.stringCalculator = new StringCalculator();
        String threeIntegers = "6,9,1001";
        //when
        int result = stringCalculator.add(threeIntegers);
        //then
        assertEquals(15, result);
    }

    @Test
    public void should_return_the_sum_of_integers_while_replacing_newline_with_coma() {
        //given
        this.stringCalculator = new StringCalculator();
        String fourIntegers = "2,9,1001\n4";
        //when
        int result = stringCalculator.add(fourIntegers);
        //then
        assertEquals(15, result);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"//;\n9;6", 15}, {"//x\n9x6", 15},});
    }

    public String input;

    public int expected;

    public TestStringCalculator(String input, int expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void should_return_the_sum_of_integers_with_custom_delimiter_option() {
        this.stringCalculator = new StringCalculator();
        assertEquals(expected, stringCalculator.add(input));
    }

    @Test
    public void should_throw_exception_when_negative_integers() throws Exception {
        // given
        this.stringCalculator = new StringCalculator();
        String twoIntegers = "//x\n-9x-6";
        try {
            //when
            int result = stringCalculator.add(twoIntegers);

            //then
            Assert.fail();
        } catch (IllegalArgumentException exception) {
        }
    }

    @Test
    public void should_return_sum_of_integers_if_newline_and_custom_delimiter (){
        //given
        this.stringCalculator = new StringCalculator();
        String integers = "//x\n1x2\n3";
        //when
        int result = stringCalculator.add(integers);
        //then
        assertEquals(6, result);
    }
}
