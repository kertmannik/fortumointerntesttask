import com.google.gson.Gson;
import ee.kert.internexercise.Transaction;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonTest {

    Gson gson = new Gson();

    @Test
    public void parse_json_to_java_object() {
        // given
        String json = "{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1000,\"year\":2015}";

        // when
        Transaction transaction = gson.fromJson(json, Transaction.class);

        // then
        assertThat(transaction.getValue()).isEqualTo(1000);
        assertThat(transaction.getYear()).isEqualTo(2015);
        assertThat(transaction.getTrader().getCity()).isEqualTo("Tartu");
        assertThat(transaction.getTrader().getName()).isEqualTo("Andrea");
    }

    @Test
    public void parse_jsonArray_to_javaArray() {

        String json = "[{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1000,\"year\":2015},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":800,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":500,\"year\":2014},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":100,\"year\":2014},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":600,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":1000,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":500,\"year\":2016},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":300,\"year\":2015},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":600,\"year\":2016}]";
        Transaction[] transactionsList = gson.fromJson(json,Transaction[].class);
        assertThat(transactionsList.length).isEqualTo(11);
    }
    @Test
    public void calculates_sum_of_transactions_from_traders_from_tartu_in_2106() {
        //given
        String json = "[{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1000,\"year\":2015},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":800,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Andrea\"},\"value\":1200,\"year\":2016},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":500,\"year\":2014},{\"trader\":{\"city\":\"Madrid\",\"name\":\"Luis\"},\"value\":100,\"year\":2014},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":600,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":1000,\"year\":2016},{\"trader\":{\"city\":\"Tartu\",\"name\":\"Tanel\"},\"value\":500,\"year\":2016},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":300,\"year\":2015},{\"trader\":{\"city\":\"London\",\"name\":\"Joe\"},\"value\":600,\"year\":2016}]";

        //when
        Transaction[] transactionsList = gson.fromJson(json, Transaction[].class);
        int sum = 0;
        if (transactionsList.length != 0) {
            for (Transaction object : transactionsList) {
                if (object.getYear() == 2016 && object.getTrader().getCity().equals("Tartu")) {
                    int value = object.getValue();
                    sum += value;
                }
            }
        }

        //then
        assertThat(sum).isEqualTo(5300);
    }
}
