package exercises;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Exercise 6.
 */
public class HttpClientTest {

    private MyHttpClient myHttpClient;

    private MockWebServer mockWebServer;

    @Before
    public void initialize() throws Exception {
        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start();

        this.myHttpClient = new MyHttpClient();
    }

    @Test
    public void should_make_post_request_to_server_with_answer_as_request_body() throws Exception {
        // given
        this.mockWebServer.enqueue(new MockResponse().setResponseCode(200));

        // when
        this.myHttpClient.isCorrectAnswer(this.mockWebServer.url("/"), "Lars on tubli poiss");

        // then
        RecordedRequest recordedRequest = this.mockWebServer.takeRequest();
        assertThat(recordedRequest.getBody().readUtf8()).isEqualTo("Lars on tubli poiss");
    }

    @Test
    public void should_return_true_when_response_contains_header_answer_correct_and_value_0() throws Exception {
        // given
        this.mockWebServer.enqueue(new MockResponse().setResponseCode(200).setHeader("answer-correct", "0"));

        // when
        final boolean isAnswerCorrect =
                this.myHttpClient.isCorrectAnswer(this.mockWebServer.url("/"), "Lars on tubli poiss");

        // then
        assertThat(isAnswerCorrect).isTrue();
    }

    class MyHttpClient {

        private final OkHttpClient client = new OkHttpClient();
        private final MediaType MEDIA_TYPE_TEXT = MediaType.parse("text/html; charset=utf-8");

        public boolean isCorrectAnswer(HttpUrl url, String answer) throws IOException {
            Request.Builder requestBuilder = new Request.Builder();
            requestBuilder.url(url);
            requestBuilder.post(RequestBody.create(MEDIA_TYPE_TEXT, answer));
            Response response = client.newCall(requestBuilder.build()).execute();
            return "0".equals(response.header("answer-correct"));
        }
    }
}
