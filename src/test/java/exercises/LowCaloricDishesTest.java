package exercises;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Exercise 3.
 */
public class LowCaloricDishesTest {

    @Test
    public void should_return_names_of_dishes_with_less_than_1800_calories() throws Exception {
        // given
        List<Dish> allDishes = new ArrayList<>();
        allDishes.add(new Dish("Buffalo Wild Wings' Cheese Curd Bacon Burger", 1950));
        allDishes.add(new Dish("The Cheesecake Factory's Flying Gorilla", 950));
        allDishes.add(new Dish("Uno Pizzeria & Grill's Chocolate Cake", 1740));
        allDishes.add(new Dish("Texas Roadhouse's 16 oz. Prime Rib", 1570));
        allDishes.add(new Dish("Dave & Buster's Carnivore Pizzadilla", 1970));
        allDishes.add(new Dish("Chili's Ultimate Smokehouse Combo", 2440));

        // when
        List<Dish> lowCaloricDishes = DishUtil.getLowCaloricDishes(allDishes);

        // then
        assertThat(lowCaloricDishes).containsExactlyInAnyOrder(
                new Dish("The Cheesecake Factory's Flying Gorilla", 950),
                new Dish("Uno Pizzeria & Grill's Chocolate Cake", 1740),
                new Dish("Texas Roadhouse's 16 oz. Prime Rib", 1570));
    }

    static class DishUtil {

        /**
         * Returns all dishes that are below 1800 calories.
         * Use either loops or streams!
         */
        public static List<Dish> getLowCaloricDishes(List<Dish> allDishes) {
            List<Dish> lowCaloricDishes = allDishes
                    .stream()
                    .filter(dish -> dish.getCalories() < 1800)
                    .collect(Collectors.toList());
            return lowCaloricDishes;
        }
    }

    static class Dish {

        final String name;

        final int calories;

        public Dish(String name, int calories) {
            this.name = name;
            this.calories = calories;
        }

        public String getName() {
            return this.name;
        }

        public int getCalories() {
            return this.calories;
        }

        // ignore everything below this line ;-)
        @Override
        public String toString() {
            return "Dish{" +
                    "name='" + name + '\'' +
                    ", calories=" + calories +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }

            Dish dish = (Dish) o;

            if (this.calories != dish.calories) {
                return false;
            }
            return this.name != null ? this.name.equals(dish.name) : dish.name == null;
        }

        @Override
        public int hashCode() {
            int result = this.name != null ? this.name.hashCode() : 0;
            result = 31 * result + this.calories;
            return result;
        }
    }
}
