package exercises;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Exercise 1.
 */
public class GsonSerializedNameTest {

    @Test
    public void should_serialize_also_json_which_uses_snake_case() throws Exception {
        // given
        final String json = "{  \"name\": \"Lars\",  \"full_name\": \"Lars Eckart\", \"familyName\":\"Eckart\" }";
        final Gson gson = new Gson();

        // when
        Person p = gson.fromJson(json, Person.class);

        // then
        assertThat(p.getName()).isEqualTo("Lars");
        assertThat(p.getFullName()).isEqualTo("Lars Eckart");
        assertThat(p.getFamilyName()).isEqualTo("Eckart");
    }

    /**
     * You are only allowed to add annotations to this class.
     * You are not allowed to rename anything in this class.
     * One annotation is all that is needed to make the test pass!
     */
    public class Person {

        private String name;
        @SerializedName("full_name") private String fullName;
        private String familyName;

        public String getName() {
            return this.name;
        }

        public String getFullName() {
            return this.fullName;
        }

        public String getFamilyName() {
            return this.familyName;
        }
    }
}
