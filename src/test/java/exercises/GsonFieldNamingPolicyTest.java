package exercises;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import org.junit.Test;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Exercise 2.
 */
public class GsonFieldNamingPolicyTest {

    /**
     * Please notice the difference in the json compared to the previous example!
     */
    @Test
    public void should_serialize_also_json_which_uses_snake_case() throws Exception {
        // given
        final String json = "{  \"name\": \"Lars\",  \"full_name\": \"Lars Eckart\", \"family_name\":\"Eckart\" }";
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create();

        // when
        Person p = gson.fromJson(json, Person.class);

        // then
        assertThat(p.getName()).isEqualTo("Lars");
        assertThat(p.getFullName()).isEqualTo("Lars Eckart");
        assertThat(p.getFamilyName()).isEqualTo("Eckart");
    }

    /**
     * You are not allowed to change anything in this class!
     */
    private static class Person {

        private String name;
        private String fullName;
        private String familyName;

        public String getName() {
            return this.name;
        }

        public String getFullName() {
            return this.fullName;
        }

        public String getFamilyName() {
            return this.familyName;
        }
    }

}
