import ee.kert.internexercise.Calculation;
import ee.kert.internexercise.Trader;
import ee.kert.internexercise.Transaction;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculationTest {

    private Calculation calc;

    @Before
    public void setUp() {
        calc = new Calculation();
    }

    @Test
    public void Should_return_0_for_empty_array() {
        //given
        Transaction[] transactions = new Transaction[0];

        //when
        int sum = calc.transactionsSum(transactions);

        //then
        assertThat(sum).isEqualTo(0);
    }

    @Test
    public void Should_return_transaction_value_when_1_transaction_from_Tartu_and_year_2016() {
        //given
        Transaction[] transactions = new Transaction[1];
        Transaction t = getTransactionMatchingCriteria();
        transactions[0] = t;

        //when
        int sum = calc.transactionsSum(transactions);

        //then
        assertThat(sum).isEqualTo(42);
    }

    private Transaction getTransaction(String city, int year) {
        Transaction t = new Transaction();
        Trader trader = new Trader();
        trader.setName("any_name");
        trader.setCity(city);
        t.setValue(42);
        t.setYear(year);
        t.setTrader(trader);
        return t;
    }

    @Test
    public void Should_return_0_when_transaction_Trader_not_from_Tartu_and_year_is_2016() {
        //given
        Transaction[] transactions = new Transaction[1];
        Transaction t = getTransaction("Paris", 2016);
        transactions[0] = t;
        //when
        int sum = calc.transactionsSum(transactions);

        //then
        assertThat(sum).isEqualTo(0);
    }

    @Test
    public void Should_return_0_when_transaction_year_is_not_2016_and_city_is_Tartu() {
        //given
        Transaction[] transactions = new Transaction[1];
        Transaction t = getTransaction("Tartu", 1999);
        transactions[0] = t;
        //when
        int sum = calc.transactionsSum(transactions);
        //then
        assertThat(sum).isEqualTo(0);
    }

    @Test
    public void Should_return_0_when_transaction_year_is_not_2016_and_city_is_not_Tartu() {
        //given
        Transaction[] transactions = new Transaction[1];
        Transaction t = getTransaction("Paris", 1999);
        transactions[0] = t;
        //when
        int sum = calc.transactionsSum(transactions);
        //then
        assertThat(sum).isEqualTo(0);
    }

    @Test
    public void Should_return_transaction_value_if_one_trader_matches_criteria_and_other_does_not() {
        //given
        Transaction[] transactions = new Transaction[2];
        Transaction t1 = getTransaction("Paris", 1999);
        Transaction t2 = getTransactionMatchingCriteria();
        transactions[0] = t1;
        transactions[1] = t2;
        //when
        int sum = calc.transactionsSum(transactions);
        //then
        assertThat(sum).isEqualTo(42);
    }

    @Test
    public void Should_return_0_if_none_of_the_traders_match_the_criteria() {
        //given
        Transaction[] transactions = new Transaction[2];
        Transaction t1 = getTransaction("Paris", 1999);
        Transaction t2 = getTransaction("Tartu", 2015);
        transactions[0] = t1;
        transactions[1] = t2;
        //when
        int sum = calc.transactionsSum(transactions);
        //then
        assertThat(sum).isEqualTo(0);
    }

    @Test
    public void Should_return_transaction_value_if_both_traders_match_criteria() {
        //given
        Transaction[] transactions = new Transaction[2];
        Transaction t1 = getTransactionMatchingCriteria();
        Transaction t2 = getTransactionMatchingCriteria();
        transactions[0] = t1;
        transactions[1] = t2;
        //when
        int sum = calc.transactionsSum(transactions);
        //then
        assertThat(sum).isEqualTo(84);
    }

    private Transaction getTransactionMatchingCriteria() {
        return getTransaction("Tartu", 2016);
    }
}