import ee.kert.internexercise.Application;
import ee.kert.internexercise.Calculation;
import ee.kert.internexercise.Client;
import ee.kert.internexercise.RawDataParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void Should_print_that_the_transaction_list_is_empty() {
        //given
        Application application = new Application();
        Client mockClient = mock(Client.class);
        when(mockClient.makeHttpRequest("http://192.168.1.49:8080/fortumo-test-task-backend/hr/task")).thenReturn("[]");
        //when
        application.doStuff(mockClient, new RawDataParser(), new Calculation());
        //then
        assertEquals("ee.kert.internexercise.Transaction list is empty.", systemOutRule.getLog());
    }
    @Test
    public void Should_print_the_transaction_sum_in_Tartu_in_2016(){
        //given
        Application application = new Application();
        Client mockClient = mock(Client.class);
        when(mockClient.makeHttpRequest("http://192.168.1.49:8080/fortumo-test-task-backend/hr/task")).thenReturn("[{\"trader\":{\"name\":\"lars\",\"city\":\"Tartu\"},\"value\":42,\"year\":2016}]");
        //when
        application.doStuff(mockClient, new RawDataParser(), new Calculation());
        //then
        assertEquals("The sum of transactions made in Tartu in 2016 is 42.", systemOutRule.getLog());
    }
}
