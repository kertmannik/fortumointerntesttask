import ee.kert.internexercise.Client;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientTest {

    private Client client;
    private MockWebServer server;

    @Before
    public void setUp() throws IOException {
        client = new Client();
        server = new MockWebServer();
        server.start();
    }

    @Test
    public void Should_return_empty_JsonArray_if_response_code_is_not_between_200_and_300() throws Exception {
        //given
        server.enqueue(new MockResponse().setResponseCode(500));
        HttpUrl baseurl = server.url("v1/chat");
        //when
        String result = client.makeHttpRequest(baseurl.toString());
        //then
        assertThat(result).isEqualTo("[]");
    }

    @Test
    public void Should_make_request_with_header() throws Exception {
        //given
        server.enqueue(new MockResponse().setResponseCode(200));
        HttpUrl baseurl = server.url("hr/task");

        //when
        client.makeHttpRequest(baseurl.toString());

        //then
        RecordedRequest request = server.takeRequest();
        String candidateHeader = request.getHeader("x-candidate");
        assertThat(candidateHeader).isEqualTo("Kert");
    }

    @Test
    public void Should_return_response_Body_as_String() throws Exception {
        //given
        server.enqueue(new MockResponse().setBody("Lars is the best!!!"));
        HttpUrl baseurl = server.url("v1/chat");
        //when
        String result = client.makeHttpRequest(baseurl.toString());
        //then
        assertThat(result).isEqualTo("Lars is the best!!!");
    }

    @Test
    public void Should_make_request_to_the_URL_from_parameter() throws Exception {
        //given
        server.enqueue(new MockResponse());
        HttpUrl baseurl = server.url("hr/task");
        //when
        client.makeHttpRequest(baseurl.toString());
        //then
        RecordedRequest request = server.takeRequest();
        String url = request.getRequestUrl().toString();
        assertThat(url).isEqualTo(baseurl.toString());
    }
}
