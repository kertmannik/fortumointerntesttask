# Introduction
This project is made to test interns abilities and knowledge, by doing exercises on the test server. Also this project is used to teach the intern new ways to write and test code.

## Purpose
This project is made to send requests to test server, print out the results, headers and return empty Json, if request is not successful.
If the response code is between 200 and 300, it will return the body. The body will be parsed with Gson and then used to calculate the sum of transactions if the person is from Tartu and year is 2016.

### TODOs
1. Documentation